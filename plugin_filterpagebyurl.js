/*jshint esversion: 6 */

const utilNet = require('./util_net');
const logger = require('./util_log').moduleLogger(module);

let coreDomain = '';

const filterByUrl = (scrapemachine, pageUrl) => {
  return new Promise((resolve, reject) => {
    // Check that the page is in the domain we are looking at
    if (!pageUrl.includes(coreDomain)) {
      scrapemachine.setPageStatus(pageUrl, scrapemachine.STATUS_LIST.OUT_OF_SCOPE);
      logger.verbose('Rejected URI ' + pageUrl + ' (not in core domain ' + coreDomain + ')');
      reject(scrapemachine, pageUrl);
      return;
    }
    logger.verbose('Accepted URI ' + pageUrl);
    resolve(scrapemachine, pageUrl);
  });
};

const getProcessFunction = (options) => {
  if (!options) options = {};
  if (!options.coreDomain) {
    errStr = 'Need coreDomain to create URL Filter plugin';
    logger.error(errStr);
    throw (errStr);
  }
  coreDomain = options.coreDomain;
  return (filterByUrl);
};

module.exports = {
  name: 'Filter by URL',
  description: 'Filters out URLs that are not within the coreDomain',
  getProcessFunction: getProcessFunction
};