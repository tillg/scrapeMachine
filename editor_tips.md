# Visual Code stuff

## Code folding

* Fold All: `Windows (Ctrl+K Ctrl+0) in Mac (Cmd+K Cmd+0)`
* Unfold All: `Windows (Ctrl+K Ctrl+J) in Mac (Cmd+K Cmd+J)`
* To see all the Shortcuts in the editor in Mac just type: `Cmd+k Cmd+S`

