/*jshint esversion: 6 */

const Scrapemachine = require('./scrapemachine');
const logger = require('./util_log').moduleLogger(module);

test('Create a Scrapemachine', () => {
  //logger.verbose('Creating ScrapeMachine');
  const myScrapemachine = new Scrapemachine('huhu', 'haha');
});

test('Set  status to non existing URL', () => {
  expect(() => {
    const myScrapemachine = new Scrapemachine('huhu', 'haha');
    myScrapemachine.setPageStatus('http://123.com', 'NEW');
  }).toThrow();
});

test('Add new pages and count them', () => {
  const myScrapemachine = new Scrapemachine('huhu', 'haha');
  myScrapemachine.addPage('page1');
  myScrapemachine.addPage('page2');
  myScrapemachine.addPage('page3');
  let noOfPages = myScrapemachine.getNoOfPages('NEW');
  expect(noOfPages).toEqual(3);
});