/*jshint esversion: 6 */

/**
 * Switching between promises and callback.
 * Taken from here:
 * https://hackernoon.com/transforming-callbacks-into-promises-and-back-again-e274c7cf7293
 */

exports.promisify = function (func) {
  return (...args) =>
    new Promise((resolve, reject) => {
      const callback = (err, data) => err ? reject(err) : resolve(data);

      func.apply(this, [...args, callback]);
    });
};

exports.callbackify = function (func) {
  return (...args) => {
    const onlyArgs = args.slice(0, args.length - 1);
    const callback = args[args.length - 1];

    func.apply(this, onlyArgs)
      .then(data => callback(null, data))
      .catch(err => callback(err));
  };
};