/*jshint esversion: 6 */

const Scrapemachine = require('./scrapemachine');

const logger = require('./util_log').moduleLogger(module);
const URL_PAGE = 'http://localhost:3000/webpage.html';
const PATH_FOR_FILES = './temp/';
const express = require('express');
const utilNet = require('./util_net');
const utilTest = require('./util_test');

// We need the express server globally available so we can properly shut it down at the end.
let expressServer;

beforeAll((done) => {
  logger.verbose('Starting local web server for testing');
  utilTest.startLocalWebServer()
    .then((newServer) => {
      expressServer = newServer;
      logger.verbose('Webserver started: ' + expressServer);
      done();
    });
});
afterAll((done) => {
  logger.verbose('Shutting down local test webserver');
  utilTest.stopLocalWebServer(expressServer)
    .then(() => {
      done();
    });
});


test('plugin load-html', (done) => {

  //logger.verbose('Creating ScrapeMachine');
  let scrapeProject = new Scrapemachine(URL_PAGE, PATH_FOR_FILES);
  scrapeProject.use('./plugin_loadhtml');
  scrapeProject.scrape()
    .then(() => {
      logger.verbose('ScrapeMachine: ' + scrapeProject.asHumanReadable());
    })
    .then(done());
});

// TODO: Make a test that tries to load a page that doesn't exist

/**
 * Starts a local web server that serves the test data we will be scraping in our tests.
 */
/* 
const startLocalWebServer = () => {

  let promise = new Promise((resolve, reject) => {
    const app = express();
    app.get('/', (req, res) => res.send('Hello World! Web server for testing is running.'));
    app.use(express.static('./testdata'));

    expressServer = app.listen(3000, () => logger.verbose('Example app listening on port 3000!'));
    resolve();
  });

  return promise;
}; */