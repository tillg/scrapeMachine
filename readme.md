# scrapeMachine

Scraping blogs and sites for _articles_. Uses a pipeline-like structure to process the content.

## Architecture / Organization

The moving parts:

### ScrapeMachine

The ScrapeMachine is the central engine.

**Creating a ScrapeMachine** is done with 2 arguments: The domain webpage where we start scraping and the location where the files will be saved.

Example:

`
let scrapeProject = new Scrapemachine(URL_PAGE, PATH_FOR_FILES);
`

**Adding Plugins** is done with the `use` verb. Plugins do the main work. They can filter, load data, transform data...

Example:

`
scrapeProject.use('./plugin_filterpagebyurl', {
    coredDomain: 'techcrunch.com'
  });
`

The first argument passed is the path to the module. In the above example the plugin referenced is expected to be in `plugin_filterpagebyurl.js` in the same directory as the ScrapeMachine class.

**Scraping** is done by chaining promises: Chained by `.then`, one plugin after the other. Every plugin returns a Promise. 
Note: The `plugin_savefiles` is special, as it also assembles promises: One promise by file that is to be written. This time the promises are grouped for parallel execution: They are added into a array of promises that is then wrapped into one promise by `Promise.all(<Array of Promises>)` .

## Reading

### About Scraping with node

* [Scraping data in 3 minutes with Javascript, Jan 27, 2017](https://medium.com/data-scraper-tips-tricks/scraping-data-with-javascript-in-3-minutes-8a7cf8275b31)
* [An Introduction to Web Scraping with Node JS, Aug 1, 2017](https://codeburst.io/an-introduction-to-web-scraping-with-node-js-1045b55c63f7)
* [Introduction to web scraping with Node.js, Jan 31, 2018](https://dev.to/aurelkurtula/introduction-to-web-scraping-with-nodejs-9h2)
* [A Guide to Automating & Scraping the Web with JavaScript (Chrome + Puppeteer + Node JS), Brandon Morelli, Oct 25, 2017](https://codeburst.io/a-guide-to-automating-scraping-the-web-with-javascript-chrome-puppeteer-node-js-b18efb9e9921) Includes a guide on how to take screen shots of Web sites.
* [Scrape Websites for Information Easily using Code.xyz and Node.js](https://codeburst.io/scrape-websites-for-information-easily-using-code-xyz-and-node-js-8be3e2f938ab)

## To do

* Describe how to build a plugin
* Add other functions as plugins: Extract URLs, extract images, transform image URLs, download images etc.
* Change all asynchronous calls to promises
* Alllow to make screen shots. Read [here](https://codeburst.io/a-guide-to-automating-scraping-the-web-with-javascript-chrome-puppeteer-node-js-b18efb9e9921)

## Log book

See commits and their comments.

## Dev Environment

To have ScrapeMachine running on your machine this is what you need:

### Mandatory

* Node v9.11.0 May be other node versions will do, but it's the only one I use & test on

### Recommended

* Visual Code v. 1.23 or higher.
* On Visual code I use a couple of helpful Extensions:
    * Beautify (HookyQR.beautify)
    * Bracket Pair Colorizer
    * EditorConfig for VS Code
    * ESLint
    * Hop Light: A nice, bright theme - very mandatory ;)
    * Import Cost: Shows you how big the `require`d files are
    * Identicator: Shows the current indentation depth
    * JavaScript Standard Style
    * Material Icon Theme
    * Node Debug 2
    * TODO Highlight (wayou.vscode-todo-highlight)
    * MarkdownLint (DavidAnson.vscode-markdownlint)

## [Music for coding](https://www.youtube.com/results?search_query=coding+music)