/*jshint esversion: 6 */

// See here:
// https://dev.to/aurelkurtula/introduction-to-web-scraping-with-nodejs-9h2

const cheerio = require('cheerio');
const fs = require('fs');
const mkdirp = require('mkdirp');
const date = require('date-and-time');
const turndownService = new require('turndown')();
const utilNet = require('./util_net');
let Scrapemachine = require('./scrapemachine');
const logger = require('./util_log').logger;

// Scrapemachine(__dirname);


// Set the global vars
const startingPage = 'https://techcrunch.com/';
const coreDomain = 'techcrunch.com';
const maxPages = 5;
let candidatePages = {};
let articlePages = {};
let rejectedPages = {};
const dataDir = __dirname + '/data';
const tempDir = __dirname + '/temp';
const processedPagesFile = dataDir + '/processedPages.json';

// Some const identifiers
const ANALYZING = 'ANALYZING';
const LOADED = 'LOADED';
const FEATURE_IMAGE = 'FEATURE_IMAGE';
const EMBEDDED_IMAGE = 'EMBEDDED_IMAGE';

scrapeProject = new Scrapemachine(startingPage, dataDir);
scrapeProject.use(require('./plugin.load_html'));
scrapeProject.scrape();


/**
 * Adds a page to the list of candidate pages and eventually invokes it's
 * analyzing & processing.
 * @param {*} url
 */
const addPageAsCandidate = url => {
  logger.verbose('addPageAsCandidate: ' + url);

  // If we have enough pages already we don't need to look at new candidates
  if (Object.keys(articlePages).length >= maxPages)
    return;

  // If we have already looked at this page we don't need to analyze it again
  if (lookedAtPage(url))
    return;

  // Check that the page is in the domain we are looking at
  if (!url.includes(coreDomain))
    return;

  // We have a valid candidate, so let's add it to our list & start analyzing it
  candidatePages[url] = null;
  analyze(url, (err) => {
    logger.verbose('addPageAsCandidate: Done with ' + url);
    if (err) {
      logger.error('addPageAsCandidate: Error when adding page ' + url + ': ' + err);
    } else {
      logger.verbose('addPageAsCandidate: Succesfully added page ' + url);
    }
  });
};

/**
 * Analyzing a page contains:
 * 1. Check that it's in the candidatePages and nobody else is working onit yet
 * 2. Set it to 'ANALYZING'
 * 3. Load it to memory
 * 4. Check wether it's an article
 *    if it is an article:
 *      Add it to articlePages
 *      Write it to disk
 *      Extract content
 */
const analyze = (url, callback) => {
  logger.verbose('analyze: Analyzing ' + url);
  let page = candidatePages[url];
  if (page && page.status) { // This would mean someone else already takes care of it
    callback('Page is already being processed.');
    return;
  }
  if (!page)
    candidatePages[url] = {
      url: url
    };
  page = candidatePages[url];

  page.status = ANALYZING;
  utilNet.loadHtml(url, (err, html) => {
    if (err) {
      logger.error('analyze: Error when loading page ' + url + ': ' + err);
      callback(err);
      return;
    }
    if (!html) {
      logger.error('analyze: Got a loadPage w/o err but HTML is null: ' + url);
      callback('Got a loadPage w/o err but returned content is null: ' + url);
      return;
    }
    logger.verbose('analyze: Loaded page ' + url);
    logger.verbose('analyze: Content:' + html.substr(1, 100));

    // Get all referenced pages and add them to the candidate page list
    extractAllRefs(html, (refs, err) => {
      if (err) {
        logger.error('analyze: Error when extracting refs: ' + err);
        callback(err);
        return;
      }
      refs.forEach(ref => {
        addPageAsCandidate(ref);
      });
    });

    if (checkForArticle(html)) {
      articlePages[url] = page;
      articlePage = articlePages[url];
      articlePage.html = html;
      page.filename = makeFilenameFromUrl(url);
      page.dirName = makeDirectoryFromUrl(url);
      articlePage.status = LOADED;
      writeResourceToDisk(articlePage.html, tempDir, articlePage.filename, (err) => {
        if (err) {
          logger.error('analyze: Error when writing article to disk: ' + articlePage.url);
          return;
        }
        extractContent(articlePage, (err, articlePage) => {
          if (err) {
            logger.error('analyze: Error when extracting content from ' + url + ': ' + err);
            callback(err);
            return;
          }
          logger.verbose('analyze: Done analyzing with ' + url);

          // Save the article as markdown
          writeArticleToDisk(articlePage, err => {
            if (err) {
              logger.error('analyze: Error writing article ' + articlePage.title + ' :' + err);
              callback(err);
              return;
            }
            delete candidatePages[url];
            callback(null, articlePage);
          });
        });
      });

    } else {
      rejectedPages[url] = {};
      delete candidatePages[url];
    }
  });
  writeUrlListsToDisk(callback);
  return;
};

/**
 * Writes an article to disk, including all it's pictures.
 * @param {} articlePage
 * @param {*} callback
 */
const writeArticleToDisk = (articlePage, callback) => {
  const fullDirName = dataDir + '/' + articlePage.dirName;

  // Write original html file, load images
  logger.verbose('writeArticleToDisk: Writing article ' + articlePage.title);
  writeResourceToDisk(articlePage.html, fullDirName, articlePage.filename, (err) => {
    if (err) {
      logger.error('writeArticleToDisk: Error writing article ' + articlePage.title + ': ' + err);
      callback(err);
      return;
    }
    //Write markdown content
    writeResourceToDisk(articlePage.content, fullDirName, articlePage.dirName + '.md', err => {
      if (err) {
        logger.error('writeArticleToDisk: Error writing article content ' + articlePage.title + ': ' + err);
        callback(err);
        return;
      }

      // Write images to disk
      // TODO: Saving images to disk doesn't work this way Use this instead:
      // https://stackoverflow.com/questions/41846669/download-an-image-using-axios-an
      // d -convert-it-to-base64/42928445
      const images = articlePage.images;
      if (images && Object.keys(images).length > 0) {
        Object
          .keys(images)
          .forEach(imageUrl => {
            logger.verbose('writeArticleToDisk: Loading & writing image ' + imageUrl);
            loadAndSaveResource(imageUrl, fullDirName, images[imageUrl].filename, err => {
              if (err) {
                logger.error('writeArticleToDisk: Error when saving image ' + imageUrl);
                callback(err);
                return;
              }
              logger.verbose('writeArticleToDisk: Successfully saved image ' + imageUrl);
            });
          });
      } else {
        callback(null, articlePage);
      }
    });
  });
};

/**
 * Writes the list of processed URLs to disk:
 * - rejectedPages
 * - candidatePages
 * - articlePages
 */
const writeUrlListsToDisk = (callback) => {

  const processedPages = {
    rejectedPages: rejectedPages,
    candidatePages: candidatePages,
    articlePages: articlePages
  };

  // Make sure the dir exists
  mkdirp(dataDir, err => {
    if (err) {
      console.error('writeUrlListsToDisk: Error: %s', error);
      callback(err);
      return;
    } else {
      fs.writeFile(processedPagesFile, JSON.stringify(processedPages), (err) => {
        callback(err);
        return;
      });

    }
  });
};

/**
 * Extracts the content from a html of an article:
 * - Date published
 * - Title
 * - Content in html
 *
 * @param {*} articlePage
 * @param {Function} callback
 */
const extractContent = (articlePage, callback) => {

  let images = {}; // Here we will store all image infos to then load & process them.

  // Get the date of our article
  const $ = cheerio.load(articlePage.html);
  const regexStr = '(\\d\\d\\d\\d_\\d\\d_\\d\\d)';
  let pubDate = articlePage
    .filename
    .match(regexStr);
  let articleDate = null;
  if (pubDate) {
    pubDate = pubDate[0]; // Result of match should be an array, we just pick the first element and should have a string now...
    const formatString = 'YYYY_MM_DD';
    if (date.isValid(pubDate, formatString)) {
      articleDate = date.parse(pubDate, formatString);
    } else {
      logger.warn('extractContent: Could not transform date from string: ' + pubDate);
    }
  }
  articlePage.date = articleDate;

  // Get the title
  const title = $('.article__title').text();
  if (title) {
    logger.verbose('extractContent: Found title: ' + title);
    articlePage.title = title;
  } else {
    logger.warn('extractContent: Did not find title of article: ' + articlePage.url);
  }

  // Get the content as HTML --> Markdown
  let contentAsHtml = $('.article-content').html();
  let contentAsMarkdown = turndownService.turndown(contentAsHtml);

  if (contentAsMarkdown) {
    articlePage.content = contentAsMarkdown;
  }

  // Get image references Get feature image
  const featureImageAsHtml = $('.article__featured-image-wrapper').html();
  if (featureImageAsHtml) {
    const $featureImage = cheerio.load(featureImageAsHtml);
    const featureImagesSrc = $('.article__featured-image').attr('src');
    const featureImageWoSize = stripSizeFromImgSrc(featureImagesSrc);
    const newImageFilename = makeFilenameFromUrl(featureImageWoSize);
    images[featureImageWoSize] = {
      url: featureImageWoSize,
      filename: newImageFilename,
      originalSrc: featureImagesSrc,
      type: FEATURE_IMAGE
    };
  } else {
    logger.warn('extractContent: Didn\'t find any feature image in ' + articlePage.url);
  }

  // Get the other (i.e. non-feature images) of the article
  const $content = cheerio.load(contentAsHtml);
  const imagesAsNodes = $content('img');
  if (imagesAsNodes.length > 0) {
    for (i = 0; i < imagesAsNodes.length; i++) {
      const node = imagesAsNodes[i];
      const immageSrc = node.attribs.src;
      const imageWoSize = stripSizeFromImgSrc(immageSrc);
      const newImageFilename = makeFilenameFromUrl(imageWoSize);
      images[imageWoSize] = {
        url: imageWoSize,
        filename: newImageFilename,
        originalSrc: immageSrc,
        type: EMBEDDED_IMAGE
      };
    }
  }
  articlePage.images = images;

  // TODO: Take care of embedded youtube videos

  // TODO: Save the images to the new file & rename the src in the HTML
  if (articlePage.images) {}

  callback(null, articlePage);
  return;
};

/**
 * Strips the size parameter out of the url of an image source.
 * @param {*} src
 */
const stripSizeFromImgSrc = src => {
  const sizeKeywords = ['w', 'width', 'h', 'height'];
  sizeKeywords.forEach(keyword => {
    const regexStr = '\\?' + keyword + '=[0-9]*';
    let matches = src.match(regexStr);
    if (matches) {
      matches.forEach(match => {
        src = src.replace(match, '');
      });
    }
  });
  return src;
};

/**
 * Extracts all references from the passed html
 * @param {String} html
 * @param {Function} callback (refs[], err)
 */
const extractAllRefs = (html, callback) => {
  let refs = [];
  const $ = cheerio.load(html);
  $('a').each(function () {
    let link = $(this).attr('href');
    logger.verbose('extractAllRefs: Found a ref: ' + link);
    if (utilNet.isValidUrl(link)) {
      refs.push(link);
    }
  });
  callback(refs, null);
  return;
};

/**
 * Writes an article to disk
 * @param {article} resourceContent to be written
 */
const writeResourceToDisk = (resourceContent, dir, filename, callback) => {
  const fullFilename = dir + '/' + filename;

  // Make sure the dir exists
  mkdirp(dir, function (err) {
    if (err) {
      console.error('writeArticleToDisk: Error when creating directory ' + dir + ': ' + error);
      callback(err);
      return;
    } else {
      fs.writeFile(fullFilename, resourceContent, err => {
        if (err) {
          logger.error('writeArticleToDisk: Error: ' + err);
          callback(err);
          return;
        } else {
          logger.verbose('writeArticleToDisk: Successfully wrote file ' + fullFilename);
          callback(null, resourceContent);
          return;
        }
      });
    }
  });
};

/**
 * Checks wether the passed html is an article.
 * @param {*} html
 */
const checkForArticle = (html) => {
  return (html.includes('<div class="article-content">'));
};

/**
 * Makes a valid filename string from a URL
 * @param {*} url
 */
const makeFilenameFromUrl = (url) => {

  // Let's save the ending for later
  let ending = url.substr(url.length - 1);
  if (ending != '/') {
    const pieces = url.split(/[.\/]+/);
    ending = pieces[pieces.length - 1];
  } else {
    ending = 'html';
  }

  // As front part of the filename we use the same logic as for building the
  // directory name
  const fronPartOfFilename = makeDirectoryFromUrl(url);
  const filename = fronPartOfFilename + '.' + ending;

  return filename;
};

/**
 * Makes a directory name as string from a URL where this article would belong
 * @param {*} url
 */
const makeDirectoryFromUrl = (url) => {
  let dirName = url;

  // Let's strip the ending
  let lastChar = url.substr(url.length - 1);
  if (lastChar === '/') {
    dirName = dirName.substr(0, dirName.length - 1);
  }

  // Strip away http(s) and //
  dirName = dirName.replace('https:', '');
  dirName = dirName.replace('http:', '');
  dirName = dirName.replace('//', '');

  // Replace all / with _
  while (dirName.includes('/')) {
    dirName = dirName.replace('/', '_');
  }

  // Replace all . with _
  while (dirName.includes('.')) {
    dirName = dirName.replace('.', '_');
  }

  // Strip away leading and trailing _
  dirName = dirName.replace(/^_+|_+$/g, '');

  return dirName;
};

/**
 * Check if we already looked at the passed URL or have it in one of our
 * lists already.
 * @param {*} url
 */
const lookedAtPage = (url) => {
  if (Object.keys(candidatePages).includes(url))
    return true;
  if (Object.keys(articlePages).includes(url))
    return true;
  if (Object.keys(rejectedPages).includes(url))
    return true;
  return false;
};



// eraseTempDir(() => addPageAsCandidate(startingPage));