/*jshint esversion: 6 */

const utilNet = require('./util_net');
const logger = require('./util_log').moduleLogger(module);

const loadHtml = (scrapemachine, pageUrl) => {
  return new Promise((resolve, reject) => {
      logger.verbose('loadHtml: Processing page ' + pageUrl);
      utilNet.downloadHtml(pageUrl)
    })
    .then((html) => {
      //logger.verbose('loadHtml: Before assigning html to scrapemachine...');
      scrapemachine.pages[pageUrl].html = html;
      //logger.verbose('loadHtml: ...after assigning html to scrapemachine.');
      logger.verbose('loadhtml: Downloaded html: ' + html.substring(0, 100));
      resolve(scrapemachine, pageUrl);
    })
    .catch(err => {
      logger.error('Error: ' + err.message);
      reject(err);
    });
};

const getProcessFunction = (options) => {
  return (loadHtml);
};

module.exports = {
  name: 'Load HTML',
  description: 'Loads the HTML into the ScrapeMachine',
  getProcessFunction: getProcessFunction
};