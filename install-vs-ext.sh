#!/bin/bash

echo 'This is "install-vs-ext.sh" adding some Visual Code specific installations'
echo 'installed VS Code extensions:'
code --list-extensions
code --install-extension PKief.material-icon-theme
code --install-extension bubersson.theme-hop-light
code --install-extension hnw.vscode-auto-open-markdown-preview
code --install-extension HookyQR.beautify
code --install-extension dbaeumer.vscode-eslint
code --install-extension leizongmin.node-module-intellisense
code --install-extension CoenraadS.bracket-pair-colorizer
code --install-extension wayou.vscode-todo-highlight
code --install-extension wix.vscode-import-cost
code --install-extension SirTori.indenticator
code --install-extension donjayamanne.githistory
code --install-extension DavidAnson.vscode-markdownlint