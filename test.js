/*jshint esversion: 6 */

const check = require("check-node-version");
const logger = require('./util_log').moduleLogger(module);
const EXPECTED_NODE_VERSION = '7.10';
const EXPECTED_NPM_VERSION = '5.6.0';

test('Check Version of Node', (done) => {
  check({
      node: "= " + EXPECTED_NODE_VERSION,
      npm: '= ' + EXPECTED_NPM_VERSION
    },
    (error, results) => {
      if (error) {
        logger.error(error);
        done(true);
        return;
      }

      if (results.isSatisfied) {
        logger.verbose("All is well.");
        done();
        return;
      }

      logger.error("Some package version(s) failed!");
      done('Package missing or wrong version!');

      for (const packageName of Object.keys(results.versions)) {
        if (!results.versions[packageName].isSatisfied) {
          logger.error(`Missing ${packageName} (or wrong version).`);
        }
      }
    }
  );


});