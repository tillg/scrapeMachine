/*jshint esversion: 6 */

const logger = require('./util_log').moduleLogger(module);
const express = require('express');

const PORT = 3000;

/**
 * Starts a local web server that serves the test data we will be scraping in our tests.
 * ASYNC. Returns a promise.
 */
exports.startLocalWebServer = () => {

  let promise = new Promise((resolve, reject) => {
    const app = express();
    app.get('/', (req, res) => res.send('Hello World! Web server for testing is running.'));
    app.use(express.static('./testdata'));

    let expressServer = app.listen(PORT, (err) => {
      if (err) {
        logger.error('Could not start local Web Server: ' + err.message);
        reject(err);
        return;
      }
      logger.verbose('Local Server listening on port ' + PORT);
      resolve(expressServer);
    });
  });

  return promise;
};

exports.stopLocalWebServer =
  expressServer => {
    let promise = new Promise((resolve, reject) => {
      if (!expressServer) {
        const errMsg = 'Cannot close web server as no server has been passed';
        logger.error(errMsg);
        throw new Error(errMsg);
      }
      expressServer.close(((err) => {
        if (err) {
          logger.error('Could not close web server: ' + err.message);
          throw err;
        } else {
          logger.verbose('Local server just closed.');
          resolve();
        }
      }));
    });
    return promise;
  };