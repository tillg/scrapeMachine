/*jshint esversion: 6 */


const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const rimraf = require('rimraf');
const logger = require('./util_log').moduleLogger(module);

/**
 * Creates the directory for the file that has been passed. ASYNC, returns a Promise.
 * Note: You pass in a filename, we create the Directory (not the file!)
 * @param {} filePath 
 */
exports.ensureDirectoryExistence = (filePath) => {
  const dirname = path.dirname(filePath);

  const promise = new Promise((resolve, reject) => {
    mkdirp(dirname, function (err) {
      //logger.verbose('ensureDirectoryExistence: returning from callback of mkdirp. Error: ' + err);
      if (err) {
        logger.error('ensureDirectoryExistence: Could not mkdirp directory for ' + filePath + ': ' + err.message);
        reject(err);
      } else {
        logger.verbose('ensureDirectoryExistence: Successfully created ' + dirname);
        resolve(dirname);
      }
    });
  });

  return promise;
};

/**
 * Erases a dir with all sub-dirs and files inside
 */
exports.eraseDir = (dirname) => {
  return new Promise((resolve, reject) => {
    rimraf(dirname, (err) => {
      if (err) {
        reject(err);
      }
      resolve(dirname);
    });
  });
};

/**
 * Checks wether a file exists. A directory also counts as file here. ASYNC, returns a Promise.
 * @param {*} filepath 
 */
exports.fileExists = (filepath) => {
  let promise = new Promise((resolve, reject) => {
    fs.stat((filepath), (err, stats) => {
      if (err) {
        return err.code === 'ENOENT' ?
          resolve(false) :
          reject(err);
      }
      resolve(stats.isFile() && stats.isDirectory());
    });
  });
  return promise;
};


/**
 * Makes a directory name as string from a URL where this article would belong
 * @param {*} url
 */
exports.makeDirectoryNameFromUrl = (url) => {
  let dirName = url;

  // Let's strip the ending
  let lastChar = url.substr(url.length - 1);
  if (lastChar === '/') {
    dirName = dirName.substr(0, dirName.length - 1);
  }

  // Strip away http(s) and //
  dirName = dirName.replace('https:', '');
  dirName = dirName.replace('http:', '');
  dirName = dirName.replace('//', '');

  // Replace all / with _
  while (dirName.includes('/')) {
    dirName = dirName.replace('/', '_');
  }

  // Replace all . with _
  while (dirName.includes('.')) {
    dirName = dirName.replace('.', '_');
  }

  // Strip out all : 
  while (dirName.includes(':')) {
    dirName = dirName.replace(':', '');
  }

  // Strip away leading and trailing _
  dirName = dirName.replace(/^_+|_+$/g, '');

  return dirName;
};



/**
 * Makes a valid filename string from a URL. The filename does not contain a path, 
 * no file extension and no other special characters than _.
 * Example http://whatever.com/index.html --> whatever_com_index
 * @param {*} url
 */
exports.makeBaseFilenameFromUrl = (url) => {

  // Let's save the ending for later
  let ending = url.substr(url.length - 1);
  let frontPartOfUrl = '';
  if (ending != '/') {
    const pieces = url.split(/[.\/]+/);
    ending = pieces[pieces.length - 1];
    frontPartOfUrl = url.substr(0, url.length - ending.length - 1);
  } else {
    frontPartOfUrl = url.substr(0, url.length - 1);
  }

  // As front part of the filename we use the same logic as for building the
  // directory name
  const filename = exports.makeDirectoryNameFromUrl(frontPartOfUrl);
  return filename;
};

/**
 * Makes a valid filename string from a URL. The filename does not contain a path, 
 * and an extension that matches the URL: html or jpg or...
 * Example http://whatever.com/index.html --> whatever_com_index.html
 * @param {*} url
 */
exports.makeFilenameFromUrl = (url) => {

  // Let's save the ending for later
  let ending = url.substr(url.length - 1);
  if (ending != '/') {
    const pieces = url.split(/[.\/]+/);
    ending = pieces[pieces.length - 1];
  } else {
    ending = 'html';
  }
  const filename = exports.makeBaseFilenameFromUrl(url) + '.' + ending;
  return filename;
};

/**
 * Writes content to disk. ASYNC, returns a Promise.
 * @param {*} resourceContent 
 * @param {*} filename including the path
 */
exports.writeResourceToDisk = (filename, resourceContent) => {
  return exports.ensureDirectoryExistence(filename)
    .then(() => {
      return new Promise((resolve, reject) => {
        fs.writeFile(filename, resourceContent, (err) => {
          if (err) {
            logger.error('writeResourceToDisk: Error: ' + err);
            reject(err);
          } else {
            logger.verbose('writeResourceToDisk: Successfully wrote file ' + filename);
            resolve(filename);
          }
        });
      });
    })
    .catch((err) => {
      logger.error('writeResourceToDisk: Error: ' + err.message);
      throw (error);
    });
};