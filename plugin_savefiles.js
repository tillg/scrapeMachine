/*jshint esversion: 6 */

const utilFile = require('./util_file');
const logger = require('./util_log').moduleLogger(module);

const saveFiles = (scrapemachine, pageUrl) => {
  return new Promise((resolve, reject) => {
    logger.verbose('Plugin saveFiles: Processing page ' + pageUrl);
    const pageDirName = scrapemachine.dirName + '/' + utilFile.makeDirectoryNameFromUrl(pageUrl);
    const pageBaseFileName = pageDirName + '/' + utilFile.makeBaseFilenameFromUrl(pageUrl);

    let promiseArray = [];

    // Loop thru the files we have
    Object.keys(scrapemachine.FILE_TYPES).forEach(fileType => {

      // Check wether file exists
      logger.verbose('Plugin saveFile: Checking on filetype ' + fileType + ' for page ' + pageUrl);
      const fileContent = scrapemachine.pages[pageUrl][scrapemachine.FILE_TYPES[fileType].attribute];
      if (fileContent) {
        const filename = utilFile.makeBaseFilenameFromUrl(pageUrl) + '.' + scrapemachine.FILE_TYPES[fileType].fileExt;
        const fullFilename = scrapemachine.directory + '/' + filename;
        logger.verbose('Saving file ' + fullFilename);
        const promise = utilFile.writeResourceToDisk(fullFilename, fileContent);
        promiseArray.push(promise);
      } else {
        logger.verbose('Plugin saveFile: Filetype not found ' + fileType + ' for page ' + pageUrl);
      }
    });
    logger.verbose('plugin_savefiles: Created array of Promises of length ' + promiseArray.length);
    let bigPromise = Promise.all(promiseArray);
    const resolvingPromise = new Promise((resolve, reject) => {
      logger.verbose('Saving all files for url finished: ' + pageUrl);
      resolve(scrapemachine, pageUrl);
    });
    bigPromise = bigPromise.then(() => {
      return resolvingPromise;
    });
    return bigPromise;
  });
};

const getProcessFunction = (options) => {
  return (saveFiles);
};


module.exports = {
  name: 'Save Files',
  description: 'Saves the files to disk',
  getProcessFunction: getProcessFunction
};