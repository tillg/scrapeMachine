/*jshint esversion: 6 */

const utilNet = require('./util_net');
const fileExists = require('file-exists');
const request = require('request');
const rimraf = require('rimraf');
const logger = require('./util_log').moduleLogger(module);
const express = require('express');
const utilAsync = require('./util_async');

// Variables we use in many tests
const PATH_FOR_FILES = __dirname + '/temp/';
const URL_PAGE = 'http://localhost:3000/webpage.html';
const URL_PIC = 'http://localhost:3000/tillgartner.jpg';

// We need the express server globally available so we can properly shut it down at the end.
let expressServer = null;

beforeAll((done) => {
  logger.verbose('Deleting temp dir');
  rimraf(PATH_FOR_FILES, () => {
    startLocalWebServer().then(done);
  });
});

afterAll(() => {
  expressServer.close();
});


test('download a HTML page with callback', (done) => {


  utilNet.downloadHtml(URL_PAGE, (err, html) => {
    if (err) {
      logger.error('Error: ' + err);
      done(err);
    } else {
      logger.verbose('Retrieved HTML: ' + html.substring(1, 300));
      done();
    }
  });
});

test('download a HTML page as promise', (done) => {

  utilNet.downloadHtml(URL_PAGE) // Omit the callback argument, so get a promise
    .then((html) => {
      logger.verbose('Downloaded HTML: ' + html.substring(1, 300));
      done();
    })
    .catch(err => {
      done(err);
    });

});


test('download a small picture thru HTTP with callback', (done) => {
  const filename = 'easyPicture_FromJestTestWithCallback.jpg';

  const callback = (err, picFullFilename) => {
    if (err) {
      logger.err('Pic could not be downloaded: ' + URL_PIC);
      done();
    }
    expect(fileExists.sync(picFullFilename)).toBeTruthy();
    done();
  };

  const downloadImageToFileWithCallback = utilAsync.callbackify(utilNet.downloadImageToFile);

  downloadImageToFileWithCallback(URL_PIC, PATH_FOR_FILES, filename, callback);
});

test('download a small picture thru HTTP with promise', (done) => {
  const filename = 'easyPicture_FromJestTestWithPromise.jpg';

  let promise = utilNet.downloadImageToFile(URL_PIC, PATH_FOR_FILES, filename);
  logger.verbose(promise);
  promise
    .then((filename) => {
      logger.verbose('Checking that file exists: ' + filename);
      done(expect(fileExists.sync(filename)).toBeTruthy());
    })
    .catch(err => {
      logger.error('Error when downloading pic: ' + err);
    });
});

/**
 * Starts a local web server that serves the test data we will be scraping in our tests.
 */
const startLocalWebServer = () => {

  let promise = new Promise((resolve, reject) => {
    const app = express();
    app.get('/', (req, res) => res.send('Hello World! Web server for testing is running.'));
    app.use(express.static('./testdata'));

    expressServer = app.listen(3000, () => logger.verbose('Example app listening on port 3000!'));
    resolve();
  });

  return promise;
};