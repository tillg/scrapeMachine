/*jshint esversion: 6 */

const assert = require('assert');
const logger = require('./util_log').moduleLogger(module);

class ScrapeMachine {
  /**
   * Creates a new ScrapeMachine.
   * @param {*} url 
   * @param {*} directory 
   */
  constructor(url, directory) {
    assert(directory, 'You must pass a working directory path.');

    this.url = url;
    this.directory = directory;
    //logger.verbose('Creating scrapemachine: url: ' + url + ' Directory: ' + directory);

    // Some constants we need
    this.STATUS_LIST = {
      NEW: 'NEW',
      OUT_OF_SCOPE: 'OUT_OF_SCOPE',
      IN_PROCESS: 'IN_PROCESS',
      PROCESSED: 'PROCESSED',
      ERROR: 'ERROR'
    };
    this.FILE_TYPES = {
      HTML: {
        attribute: 'html',
        fileExt: 'html'
      },
      MARKDOWN: {
        attribute: 'markdown',
        fileExt: 'md'
      }
    };

    // Some internal variables we need
    this.plugins = [];
    this.pages = {};

    // Some default vars we need
    this.maxProcessed = 10;
    this.maxInProcess = 10; // With these constanst we should get a max of 20 pages processed in one go
  }

  /**
   * Sets the status of a page. SYNCHRONOUS
   * @param {*} pageUrl 
   * @param {*} newStatus 
   */
  setPageStatus(pageUrl, newStatus) {
    // Check that the page is in our list
    if (!this.pages[pageUrl]) {
      const errorStr = 'Attempt to change URL status of unkown URL: ' + pageUrl;
      logger.error(errorStr);
      throw errorStr;
    }
    // Check that status is correct
    if (!Object.keys(this.STATUS_LIST).includes(newStatus)) {
      const errorStr = 'Attempt to set a URL to an invalid status: ' + newStatus;
      logger.error(errorStr);
      throw errorStr;
    }
    // Change it's status
    this.pages[pageUrl].status = newStatus;
    return;
  }

  /**
   * Adds a page to our list of pages to scrape. SYNCHRONOUS
   * @param {} pageUrl 
   */
  addPage(pageUrl) {
    // If URL already exists we are done
    if (this.pages[pageUrl]) return;
    this.pages[pageUrl] = {
      status: this.STATUS_LIST.NEW
    };
  }

  /**
   * Returns the number of pages in a certain status. SYNCHRONOUS
   * @param {*} statusToCount 
   */
  getNoOfPages(statusToCount) {
    if (!statusToCount) return this.pages.length; // If we ask w/o status, we  return the no of pages
    const reducer = (accumulator, currentValue) => {
      if (currentValue.status == statusToCount) accumulator++;
      return accumulator;
    };
    return Object.values(this.pages).reduce(reducer, 0);
  }

  /**
   * Returns a string that describes the ScrapeManchine instance so a human can read it. SYNCHRONOUS
   */
  asHumanReadable() {
    const NL = '\n';
    let desc = 'ScrapMachine description: ' + NL;
    desc += 'url: ' + this.url + NL;
    desc += 'Directory: ' + this.directory + NL;
    desc += 'No of plugins: ' + this.plugins.length + NL;
    this.plugins.forEach(plugin => {
      desc += plugin.name + NL;
    });
    desc += NL;
    desc += 'URLs:' + NL;
    Object.keys(this.STATUS_LIST).forEach(status => {
      desc += '   ' + status + ': ' + this.getNoOfPages(status) + NL;
    });
    return desc;
  }

  /**
   * Adds a plugin into the process chain. SYNCHRONOUS.
   * Returns nothing.
   * @param {*} pluginFile 
   * @param {*} options 
   */
  use(pluginFile, options) {
    const pluginObject = require(pluginFile);

    // Check that plugin is a function
    if (!pluginObject.getProcessFunction) {
      const errStr = 'Cannot add a plugin that does not contain getProcessFunction: ' + pluginFile;
      logger.error(errStr);
      throw errStr;
    }
    if (!options) options = {};
    const processFunction = pluginObject.getProcessFunction(options);

    if (typeof processFunction !== "function") {
      const errStr = 'Cannot add a plugin that is not a function: ' + pluginFile;
      logger.error(errStr);
      throw errStr;
    }
    // plugin into the stack / array
    let processObject = pluginObject;
    processObject.processFunction = processFunction;
    this.plugins.push(processObject);
  }

  /**
   * Launches the scraping project. ASYNC, returns a Promise.
   */
  scrape() {
    //logger.verbose('ScrapeMachine.scrape: Entering scrape...');
    this.addPage(this.url);
    let processAllUrlsPromise = new Promise((resolve, reject) => {
      logger.verbose('Starting promise chain');
      resolve();
    });
    Object.keys(this.pages).forEach(url => {
      processAllUrlsPromise = processAllUrlsPromise.then(() => {
        return this.process(url);
      });
      processAllUrlsPromise = processAllUrlsPromise
        .then((result) => {
          logger.verbose('Done scraping');
          return result;
        })
        .catch(err => logger.error('Error scraping: ' + err));
    });
    return processAllUrlsPromise;
  }


  /**
   * Processes the page passed. ASYNC, returns Promise
   * @param {} pageUrl 
   */
  process(pageUrl) {
    logger.verbose('Start processing page ' + pageUrl);
    // If the URL is not listed as NEW something is wrong
    if (!this.pages[pageUrl]) {
      const errStr = 'Trying to process unlisted URL: ' + pageUrl;
      logger.error(errStr);
      throw errStr;
    }
    if (this.pages[pageUrl].status !== this.STATUS_LIST.NEW) {
      const errStr = 'Trying to process URL that is not of status "NEW": ' + pageUrl;
      logger.error(errStr);
      throw errStr;
    }

    // Check that we have not yet reached the limit of pages we can process in parallel
    if (this.getNoOfPages(this.STATUS_LIST.IN_PROCESS) > this.maxInProcess) {
      const warnStr = 'Cannot process page as too many pages IN_PROCESS: ' + pageUrl;
      logger.warn(warnStr);
      return (this, pageUrl);
    }

    this.setPageStatus(pageUrl, this.STATUS_LIST.IN_PROCESS);
    let promiseChain = new Promise((resolve, reject) => {
      logger.verbose('Initiating promise chain for ' + pageUrl);
      resolve(this, pageUrl);
    });

    // TODO: Loop through the plugins and process the one page
    // What if one promise in the chain rejects? This 
    //  cleanly stops processing the file and continue to the next one.
    for (let i = 0; i < this.plugins.length; i++) {
      logger.verbose('process: Adding plugin to the chain: ' + this.plugins[i].name) + 'for ' + pageUrl;
      const processFunction = this.plugins[i].processFunction;
      promiseChain = promiseChain.then(() => processFunction(this, pageUrl));
      promiseChain = promiseChain.then(() => {
        logger.verbose('Resolving promise of plugin ');
        logger.verbose(this.plugins[i].name + ' for page ' + pageUrl);
      });
    }
    promiseChain = promiseChain.then(() => {
        logger.verbose('Finished scraping page ' + pageUrl);
        this.setPageStatus(pageUrl, this.STATUS_LIST.PROCESSED);
        return (this, pageUrl);
      })
      .catch(err => {
        logger.verbose('Page processing has been interrupted: ' + err.message);
        if (this.pages[pageUrl].status == this.STATUS_LIST.IN_PROCESS) {
          this.setPageStatus(pageUrl, this.STATUS_LIST.ERROR);
        }
        return (this, pageUrl);
      });

    return promiseChain;
  }
}

module.exports = ScrapeMachine;