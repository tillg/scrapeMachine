/*jshint esversion: 6 */

const utilFile = require('./util_file');
const fs = require('fs');
const util = require('./util');
const logger = require('./util_log').moduleLogger(module);
const rimraf = require('rimraf');
const mkdirp = require('mkdirp');
const path = require('path');

const tempDir = __dirname + '/temp';

beforeAll((done) => {
    logger.verbose('Deleting temp dir: ' + tempDir);
    rimraf(tempDir, () => {
        done();
    });
    done();
});

test('Create a nested directory with promise', (done) => {
    const nestingLevels = util.getRandomInt(4) + 1; // A random int between 1-4

    const filename = './temp/huhu/haha/hihi/supi.txt';

    utilFile.ensureDirectoryExistence(filename)
        .then((directory) => {
            logger.verbose('Directory should exist: ' + directory);
            return utilFile.fileExists(directory);
        })
        .then(fileExists => done())
        .catch(_ => done('File doesnt exist'));
});

test('Create a filename based on a URL', () => {
    const url1 = 'http://whatever.com/index.html';
    const expectedFilename1 = 'whatever_com_index.html';
    const calculatedFileName1 = utilFile.makeFilenameFromUrl(url1);
    logger.verbose(calculatedFileName1);
    expect(calculatedFileName1).toBe(expectedFilename1);

    const url2 = 'http://whatever.com/index/';
    const expectedFilename2 = 'whatever_com_index.html';
    const calculatedFileName2 = utilFile.makeFilenameFromUrl(url2);
    logger.verbose(calculatedFileName2);
    expect(calculatedFileName2).toBe(expectedFilename2);

});

test('Create a base filename based on a URL', () => {
    const url = 'http://whatever.com/index.html';
    const expectedFilename = 'whatever_com_index';
    const calculatedFileName = utilFile.makeBaseFilenameFromUrl(url);
    //logger.verbose(calculatedFileName);
    expect(calculatedFileName).toBe(expectedFilename);

    const url2 = 'http://localhost:3000/webpage.html';
    const expectedFilename2 = 'localhost3000_webpage';
    const calculatedFileName2 = utilFile.makeBaseFilenameFromUrl(url2);
    //logger.verbose(calculatedFileName2);
    expect(calculatedFileName2).toBe(expectedFilename2);

});

test('Save a file with a promise', (done) => {
    const filename = tempDir + '/whatever.txt';
    const fileContent = `Kirchalicht, Luada, Hampara, du bist doch af da Brennsuppn daheagschwomma, Flegel, Badhur, gscherta Hamml, halbseidener, Gscheidal, varreckter Hund, Affnasch, Scheißbürschdl, aufgschdeida Mausdreg, Aff, Wurznsepp, Schoaswiesn, Voglscheicha, varreckter Hund, Giftschbridzn, Wuidsau, Fettl, boaniga, Drottl, gscheada Saubreiß, Gschpusi, junga Duttara, oide Rudschn, du saudamischa, Bierdimpfl, Flaschn, krummhaxata Goaßbog, misdiga Lausbua, Kircharutschn, Hundsgribbe, Bodschal, Betonschedl, oida Daggl, varreckta Deifi, klebrigs Biaschal, di hams midam Stickl Brot ausm Woid raußzogn, Schanial, Hallodri, Hundsgribbe, Fieschkoobf, Schuibuamtratza, Schbinodwachdl, fade Noggn, Affnasch, Fünferl, glei fangst a boa!
Schbinodwachdl, Goaspeterl, Apruiaff, Heislmo, Zeeefix, Hundsgribbe, Freindal, Himmeheagodna, Apruiaff, Flegel, Rotzgloggn, Dramhappada, Gschpusi, hoid dei Babbn, Pimpanell, Schachtlhuba, Beißzanga, boaniga, Bauernfünfa, Hinderducker, Krippnmandl, Hundsgribbe, Umstandskrama, gscheada Saubreiß, Brunzkachl, du ogsoachte, Schbruchbeidl, Broatarsch, Betschwesta, Presssack, Schoaswiesn, Hoibschaariga, Schuggsn, Vieh mit Haxn, gwampate Sau, Hundsgribbe, i glaub, dir brennt da Huat, du saudamischa, Palmesel, Umstandskrama, Zuchtl, Lausbua, Schuibuamtratza, Schnoin, Schusterbua, Goaspeterl, Auftreiwa, Pfingsdochs, Biaschdal, Grattla, Flaschn!
Brunzkachl, du ogsoachte, du ogsoachte, Gscheidal, Rutschn, Pfingsdochs, Nasnboara, Randstoamare, gscherte Nuss, Rabenviech, Geizgroogn, Finessenseppal, Pfundhamml, Zipfebritschn, Kirchalicht, Sau, Bazi, Blattada, aus’gschammta, Kasberlkopf, elendiger, Betonschedl, Schdinkadores, gscherte Nuss, Finessenseppal, schdaubiga Bruada, Katzlmacha, du Ams’l, du bleede, Himbeerdoni, Haumdaucha, mogsd a Wadschn, Stodara, Saubreiß, japanischer, oide Rudschn, Zipfebritschn, Biaschdal, Wuidsau, Hämmoridenpritschn, Zuagroasta, Honigscheißa, Großkopfada, Fegeisen, Aufgeiga, Affnasch, Lausbua, di hams midam Stickl Brot ausm Woid raußzogn, Saubreiß, Frichdal, Zigarettnbiaschal, oide Rudschn, Zwidawurzn!`;

    utilFile.writeResourceToDisk(filename, fileContent)
        .then((filename) => {
            logger.verbose('Test: File written: ' + filename);
            return utilFile.fileExists(filename);
        })
        .then((fileExists) => {
            done(fileExists)
        })
        .catch(err => done(err));

});