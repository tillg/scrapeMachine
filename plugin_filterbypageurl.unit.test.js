/*jshint esversion: 6 */

const Scrapemachine = require('./scrapemachine');

const logger = require('./util_log').moduleLogger(module);
const URL_PAGE = 'http://localhost:3000/webpage.html';
const PATH_FOR_FILES = './temp/';
const express = require('express');
const utilNet = require('./util_net');

// We need the express server globally available so we can properly shut it down at the end.
let expressServer = null;

beforeAll((done) => {
  startLocalWebServer().then(done);
});
afterAll(() => {
  expressServer.close();
});


test('Run Scrapemachine plugin filter-by-page-url that rejects a URL', (done) => {

  logger.verbose('Creating ScrapeMachine');
  let scrapeProject = new Scrapemachine(URL_PAGE, PATH_FOR_FILES);
  scrapeProject.use('./plugin_filterpagebyurl', {
    coreDomain: 'huhu'
  });
  scrapeProject.scrape()
    .then(() => {
      expect(scrapeProject.getNoOfPages('OUT_OF_SCOPE')).toBe(1);
      //logger.verbose('ScrapeMachine: ' + scrapeProject.asHumanReadable());
    })
    .then(() => done());
});

test('Run Scrapemachine plugin filter-by-page-url that accepts a URL', (done) => {

  logger.verbose('Creating ScrapeMachine');
  let scrapeProject = new Scrapemachine('http://huhu.com', PATH_FOR_FILES);
  scrapeProject.use('./plugin_filterpagebyurl', {
    coreDomain: 'huhu.com'
  });
  scrapeProject.scrape()
    .then(() => {
      logger.verbose('ScrapeMachine: ' + scrapeProject.asHumanReadable());
      expect(scrapeProject.getNoOfPages('PROCESSED')).toBe(1);
    })
    .then(_ => done());
});

test('Run Scrapemachine plugin filter-by-page-url that accepts and rejects URLs', (done) => {

  logger.verbose('Creating ScrapeMachine');
  let scrapeProject = new Scrapemachine('http://huhu.com', PATH_FOR_FILES);
  scrapeProject.use('./plugin_filterpagebyurl', {
    coreDomain: 'huhu.com'
  });
  scrapeProject.addPage('http://huhu.com/whatever1');
  scrapeProject.addPage('http://huhu.com/whatever2');
  scrapeProject.addPage('http://anotherdomain.com/whatever2');
  scrapeProject.addPage('http://huhu.com/whatever3');
  scrapeProject.scrape()
    .then(() => {
      logger.verbose('ScrapeMachine: ' + scrapeProject.asHumanReadable());
      expect(scrapeProject.getNoOfPages('PROCESSED')).toBe(4); // huhu.com and 3 we added
      expect(scrapeProject.getNoOfPages('OUT_OF_SCOPE')).toBe(1);
    })
    .then(_ => done());
});


/**
 * Starts a local web server that serves the test data we will be scraping in our tests.
 */
const startLocalWebServer = () => {

  let promise = new Promise((resolve, reject) => {
    const app = express();
    app.get('/', (req, res) => res.send('Hello World! Web server for testing is running.'));
    app.use(express.static('./testdata'));

    expressServer = app.listen(3000, () => logger.verbose('Example app listening on port 3000!'));
    resolve();
  });

  return promise;
};