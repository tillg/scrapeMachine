/*jshint esversion: 6 */

const Scrapemachine = require('./scrapemachine');

const logger = require('./util_log').moduleLogger(module);
const URL_PAGE = 'http://localhost:3000/webpage.html';
const PATH_FOR_FILES = './temp/';
const utilNet = require('./util_net');
const utilTest = require('./util_test');


// We need the express server globally available so we can properly shut it down at the end.
let expressServer;

beforeAll((done) => {
  logger.verbose('Starting local web server for testing');
  utilTest.startLocalWebServer()
    .then((newServer) => {
      expressServer = newServer;
      logger.verbose('Local webserver running: ' + expressServer);
      done();
    })
    .catch(err => {
      logger.error('Cannot start local web server: ' + err.message);
      done(false);
      throw (err);
    });
});

afterAll((done) => {
  logger.verbose('Shutting down local test webserver');
  utilTest.stopLocalWebServer(expressServer)
    .then(() => {
      logger.verbose('Web server closed');
      done();
    });
});


test('Run plugin saveFiles', (done) => {

  //logger.verbose('Creating ScrapeMachine');
  let scrapeProject = new Scrapemachine(URL_PAGE, PATH_FOR_FILES);
  scrapeProject.use('./plugin_loadhtml');
  scrapeProject.use('./plugin_savefiles');
  scrapeProject.scrape()
    .then(() => {
      logger.verbose('ScrapeMachine: ' + scrapeProject.asHumanReadable());
      done();
    })
    .catch(err => {
      logger.error('Test Run plugin saveFiles: Error: ' + err.message);
    });
});